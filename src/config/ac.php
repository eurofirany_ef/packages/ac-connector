<?php

return [
    'login' => env('AC_LOGIN'),
    'password' => env('AC_PASSWORD'),
    'password_ac_write' => env('AC_PASSWORD_WRITE'),
    'url' => [
        'product-read' => env('AC_PRODUCT_READ_URL', ''),
        'product-write' => env('AC_PRODUCT_WRITE_URL', ''),
        'tema-write' => env('TEMA_WRITE_URL', '')
    ],

    /** Queue Settings */
    'queue' => [
        'enabled' => false, // Is queue for connector enabled
        'max_tries' => 3, // Max tries for failed job
        'max_running_jobs' => 5, // Max running jobs at the same time
        'max_wait_seconds' => 300, // Wait seconds when there are too many jobs
        'sleep_seconds' => 10, // Seconds to wait between attempts
        'max_per_minute' => 0 // Max jobs run per minute, 0 for no limit
    ]
];

<?php

namespace Eurofirany\AcConnector;

use Illuminate\Support\Collection;

/**
 * @property Api api
 */
class Connector
{
    /**
     * Connector constructor.
     */
    public function __construct()
    {
        // Create api
        $this->api = new Api();
    }

    /**
     * @param array $productSKU
     * @param bool $omitCache
     * @param int $rangeFrom
     * @param int $rangeTo
     * @return Collection
     */
    public function getProducts(
        array $productSKU = [],
        bool  $omitCache = false,
        int   $rangeFrom = 0,
        int   $rangeTo = 0
    ): Collection
    {
        // Add product sku if requested
        if (!empty($productSKU)) {
            foreach ($productSKU as $product)
                $this->api->addParameter('kodytema', ['kod_tema' => $product]);
        } else
            $this->api->setParameter('kodytema', []);

        $this->api->setParameter('KodTemaOd', $rangeFrom);
        $this->api->setParameter('KodTemaDo', $rangeTo);

        // Get products
        $products = $this->api->request('Slownik', $omitCache ? 0 : 3600, $omitCache);

        // Return parsed collection
        if (count($productSKU) === 1)
            return collect([$products->SlownikResult->lista->SlownikPozycja])->keyBy('kod_tema');
        else
            return collect($products->SlownikResult->lista->SlownikPozycja)->keyBy('kod_tema');
    }

    /**
     * @return mixed
     */
    public function getGroupsAndAssortment(): mixed
    {
        return $this->api->request('Grupy');
    }

    /**
     * @param array $order
     * @return mixed
     */
    public function sendTemaOrder(array $order): mixed
    {
        $this->api->setParameter('oZamowienieOdb', $order);

        return $this->api->request('TemaZamowienie', 0, 1, 2);
    }

    /**
     * @param array $order
     * @return mixed
     */
    public function sendOrder(array $order): mixed
    {
        $this->api->setParameter('Tryb', 'P');
        $this->api->setParameter('ZamowienieOdb', $order);

        return $this->api->request('ZamowienieDodaj', 0, 1, 1);
    }

    /**
     * @param string $storeCode
     * @param string $orderId
     * @return mixed
     */
    public function sendOrderStatus(string $storeCode, string $orderId): mixed
    {
        $this->api->setParameter('cIdKodMag', $storeCode);
        $this->api->setParameter('cIdZewZamow', $orderId);
        $this->api->setParameter('cRodzaj', 'RO');

        return $this->api->request('TemaStatusZamowienia', 0, 1, 2);
    }

    /**
     * @param string[]|int[] $productsSku
     * @param string $priceBranch
     * @param array $branches
     * @param array $markets
     * @param string $priceField
     * @return mixed
     */
    public function getPricesAndStocks(
        array  $productsSku = [],
        string $priceBranch = '',
        array  $branches = [],
        array  $markets = [],
        string $priceField = 'CENA_P'
    ): mixed
    {

        $this->api->setParameter('FiliaPrice_Netto', $priceBranch);
        $this->api->setParameter('Filie', $branches);
        $this->api->setParameter('PS_Pole', '');
        $this->api->setParameter('Kody', $productsSku);
        $this->api->setParameter('Pole_Ceny', $priceField);
        $this->api->setParameter('Rynki', $markets);

        return $this->api->request('StanyPS', 0, 1, 1);
    }

    /**
     * @param string $branchCode
     * @param array $ordersNumbers
     * @return mixed
     */
    public function getOrdersPackages(string $branchCode, array $ordersNumbers): mixed
    {
        $this->api->setParameter('NrListuPrzewozowego', [
            'aTabPozycje' => $ordersNumbers,
            'KodFilii' => $branchCode,
            'status' => true
        ]);

        return $this->api->request('NrListuPrzewozowego', 0, 1, 1);
    }

    /**
     * @param array $orders
     * @return mixed
     */
    public function sendPayuPaymentData(array $orders): mixed
    {
        $this->api->setParameter('ListaZamowien', $orders);

        return $this->api->request('PlatnoscPayuSeryjna', 0, 1, 1);
    }

    /**
     * @return mixed
     */
    public function getCollectionRelations(): mixed
    {
        return $this->api->request('Listingi');
    }

    /**
     * @param string[] $contractorsCodes
     * @return mixed
     * @throws \Exception
     */
    public function getPrices(array $contractorsCodes): mixed
    {
        foreach ($contractorsCodes as $contractorCode)
            $this->api->addParameter('Kontrahenci', ['numer' => $contractorCode]);

        return $this->api->request('Marzownica', 0, 1, 0);
    }

    /**
     * Set if you want or not to ignore queue for next requests
     * @param bool $ignoreQueueForRequests
     */
    public function setIgnoreQueueForRequests(bool $ignoreQueueForRequests)
    {
        $this->api->setIgnoreQueueForRequests($ignoreQueueForRequests);
    }

    /**
     * Test connector
     * @param int $endpoint Endpoint number to check
     * @return mixed
     * @throws \Exception
     */
    public function test(int $endpoint = 0): mixed
    {
        return $this->api->request('Wersja', 0, 1, $endpoint);
    }
}
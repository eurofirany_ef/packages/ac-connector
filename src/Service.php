<?php

namespace Eurofirany\AcConnector;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use stdClass;

class Service
{
    /**
     * @var Connector
     */
    private Connector $connector;

    /**
     * Service constructor.
     */
    public function __construct()
    {
        $this->connector = new Connector();
    }

    /**
     * Get all products from AC with All Details
     * @return Collection
     */
    public function getAllProductWithAllDetails(): Collection
    {
        return $this->getProductsWithDetails([], 1);
    }

    /**
     * Get products for SKU array or for range from AC with All Details
     * @param array $productSKU Array of products sku which will be taken
     * @param bool $omitCache If true, script will skip cache
     * @param int $rangeFrom Number of sku from which products are to be taken
     * @param int $rangeTo Number of sku to which products are to be taken
     * @return Collection
     */
    public function getProductsWithDetails(
        array $productSKU = [],
        bool  $omitCache = false,
        int   $rangeFrom = 0,
        int   $rangeTo = 0
    ): Collection
    {
        $assortment = $this->getAssortment();
        list($groups, $subgroups) = $this->getGroupsAndSubgroups();

        // Get products data
        $products = $this->connector->getProducts($productSKU, $omitCache, $rangeFrom, $rangeTo);
        if (!$products)
            return collect([]);

        foreach ($products as $key => $product) {
            $products[$key]->{'Asortyment'} = $assortment[$product->Grupa . '_' . $product->Podgrupa] ?? null;
            $products[$key]->{'Grupa_Nazwa'} = $groups[$product->Grupa] ?? null;
            $products[$key]->{'Podgrupa_Nazwa'} = $subgroups[$product->Grupa . '_' . $product->Podgrupa] ?? null;
        }

        return collect($products);
    }

    /**
     * Get products for SKU array or for range from AC without details
     * @param array $productSKU Array of products sku which will be taken
     * @param bool $omitCache If true, script will skip cache
     * @param int $rangeFrom Number of sku from which products are to be taken
     * @param int $rangeTo Number of sku to which products are to be taken
     * @return Collection
     */
    public function getProducts(
        array $productSKU = [],
        bool  $omitCache = false,
        int   $rangeFrom = 0,
        int   $rangeTo = 0
    ): Collection
    {
        $productSKU = collect($productSKU)->filter(function ($sku) {
            return Str::length($sku) <= 6;
        })->unique();

        if ($productSKU->count() === 0)
            return new Collection();

        return $this->connector->getProducts($productSKU->toArray(), $omitCache, $rangeFrom, $rangeTo);
    }

    /**
     * Get group and assortment from AC
     * @return array
     */
    public function getAssortment(): array
    {
        $groupsAndAssortments = $this->connector->getGroupsAndAssortment()->GrupyResult->lista->GrupyPozycja;

        foreach ($groupsAndAssortments as $group) {
            if (!empty(trim($group->nazwa_zew))) {
                $assortments[$group->grupa . '_' . $group->podgrupa] = $group->nazwa_zew;
                $nameParts = explode('\\', $group->nazwa_zew);
                $mains[$nameParts['0']] = $nameParts['0'];
                $subgroups[$nameParts['0'] . '\\' . $nameParts['1']] = $nameParts['0'] . '\\' . $nameParts['1'];
            }
        }

        return ($assortments ?? []) + ($mains ?? []) + ($subgroups ?? []);
    }

    /**
     * Get listing
     * @return mixed
     */
    public function getCollectionRelations(): mixed
    {
        return $this->connector->getCollectionRelations()->ListingiResult;
    }

    /**
     * Get possible groups and subgroups
     * @return array[]
     */
    private function getGroupsAndSubgroups(): array
    {
        $groupsAndSubGroups = $this->connector->getGroupsAndAssortment()->GrupyResult->lista->GrupyPozycja;

        foreach ($groupsAndSubGroups as $groupAndSubGroup) {
            $groups[$groupAndSubGroup->grupa] = ucfirst(mb_strtolower($groupAndSubGroup->nazwa));
            $subgroups[$groupAndSubGroup->grupa . '_' . $groupAndSubGroup->podgrupa] = ucfirst(mb_strtolower($groupAndSubGroup->nazwa_pg));
        }

        return [$groups ?? [], $subgroups ?? []];
    }

    /**
     * Get all products between sku numbers
     * @param int $from Number of sku from which products are to be taken
     * @param int $to Number of sku to which products are to be taken
     * @return Collection
     */
    public function getProductsByRange($from, $to): Collection
    {
        return $this->getProducts([], 0, $from, $to);
    }

    /**
     * Send order to Tema endpoint
     * @param array $order Order data
     * @return mixed
     */
    public function sendTemaOrder(array $order): mixed
    {
        return $this->connector->sendTemaOrder($order);
    }

    /**
     * Send order to AC endpoint
     * @param array $order Order data
     * @return mixed
     */
    public function sendOrder(array $order): mixed
    {
        return $this->connector->sendOrder($order);
    }

    /**
     * Send status for order
     * @param string $storeCode Store code of order
     * @param string $orderId Order number
     * @return array|object
     */
    public function sendOrderStatus(string $storeCode, string $orderId): object|array
    {
        return $this->connector->sendOrderStatus($storeCode, $orderId);
    }

    /**
     * Get prices and stocks from AC
     * @param Collection $productsSku Sku of product for which prices and stocks will be taken
     * @param string $priceBranch Branch for prices
     * @param array $branches Products branches
     * @param array $markets Markets for product
     * @param string $priceField Field of price to take
     * @return Collection
     */
    public function getPricesAndStocks(
        Collection $productsSku,
        string     $priceBranch = '',
        array      $branches = [],
        array      $markets = [],
        string     $priceField = 'CENA_P'
    ): Collection
    {
        $branchesArray = [];
        foreach ($branches as $branch) {
            $branchObj = new stdClass();
            $branchObj->filia = $branch;
            $branchesArray[] = $branchObj;
        }

        $marketsArray = [];
        foreach ($markets as $market) {
            $marketObj = new stdClass();
            $marketObj->kod_rynku = $market;
            $marketsArray[] = $marketObj;
        }

        $productsSkuArrays = $productsSku->unique()->chunk(1000);

        $dataArray = [];
        /** @var Collection $productSkuArray */
        foreach ($productsSkuArrays as $productSkuArray) {
            $skuArray = [];
            foreach ($productSkuArray->toArray() as $productSku) {
                $skuObj = new stdClass();
                $skuObj->kod_tema = $productSku;
                $skuArray[] = $skuObj;
            }

            $data = $this->connector->getPricesAndStocks(
                $skuArray,
                $priceBranch,
                $branchesArray,
                $marketsArray,
                $priceField
            );

            if (isset($data->StanyPSResult->lista->PozycjaPS))
                if (!is_array($data->StanyPSResult->lista->PozycjaPS))
                    $dataArray = array_merge($dataArray, [$data->StanyPSResult->lista->PozycjaPS]);
                else
                    $dataArray = array_merge($dataArray, $data->StanyPSResult->lista->PozycjaPS);
        }

        return collect($dataArray);
    }

    /**
     * Get orders packages from AC
     * @param string $branchCode Branch of order code
     * @param Collection $ordersNumbersCollection Collection of orders numbers
     * @return Collection
     */
    public function getOrdersPackages(string $branchCode, Collection $ordersNumbersCollection): Collection
    {
        $allOrdersPackages = [];

        /** @var Collection $ordersNumbers */
        foreach ($ordersNumbersCollection->chunk(1000) as $ordersNumbers) {
            $orderPackage = $this->connector->getOrdersPackages($branchCode, $ordersNumbers->toArray());

            if (isset($orderPackage->NrListuPrzewozowegoResult->aTabPozycje->NrListuPozycjeStruct)) {
                $ordersPackages = $orderPackage->NrListuPrzewozowegoResult->aTabPozycje->NrListuPozycjeStruct;

                $allOrdersPackages = array_merge(
                    $allOrdersPackages,
                    is_array($ordersPackages) ? $ordersPackages : [$ordersPackages]
                );
            }
        }

        return collect($allOrdersPackages);
    }

    /**
     * Set PayU Payment status for AC orders
     * @param array $ordersData Orders data to update [order_id, payment_number]
     * @return mixed
     */
    public function sendPayuPaymentsDataForOrders(array $ordersData): mixed
    {
        foreach ($ordersData as $orderData) {
            $order = new stdClass();
            $order->ZewNrZamowienia = $orderData['order_id'];
            $order->NrPlatnosci = $orderData['payment_number'];

            $orders[] = $order;
        }

        return $this->connector->sendPayuPaymentData($orders ?? []);
    }

    /**
     * Set PayU Payment data for AC order
     * @param string $orderId Order ID to be updated
     * @param string $paymentNumber Payment number
     * @return mixed
     */
    public function sendPayuPaymentDataForOrder(string $orderId, string $paymentNumber = ''): mixed
    {
        $order = new stdClass();
        $order->ZewNrZamowienia = $orderId;
        $order->NrPlatnosci = $paymentNumber;

        $orders[] = $order;

        return $this->connector->sendPayuPaymentData($orders);
    }

    /**
     * Get prices for contractors
     * @param string[] $contractorCodes Codes of contractors for which prices will be taken
     * @return mixed
     * @throws Exception
     */
    public function getContractorPrices(array $contractorCodes): mixed
    {
        return $this->connector->getPrices($contractorCodes)->MarzownicaResult->lista;
    }

    /**
     * Set if you want or not to ignore queue for next requests
     * @param bool $ignoreQueueForRequests
     */
    public function setIgnoreQueueForRequests(bool $ignoreQueueForRequests)
    {
        $this->connector->setIgnoreQueueForRequests($ignoreQueueForRequests);
    }

    /**
     * Test connector
     * @param int $endpoint Endpoint number to check
     * @return mixed
     * @throws Exception
     */
    public function test(int $endpoint = 0): mixed
    {
        return $this->connector->test($endpoint);
    }
}

<?php

namespace Eurofirany\AcConnector\Providers;

use Illuminate\Support\ServiceProvider;

class AcConnectorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/ac.php', 'ac_config');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/ac.php' => app()->configPath('ac.php'),
        ], 'ac_config');
    }
}

<?php

namespace Eurofirany\AcConnector;

use Eurofirany\ConnectorsQueue\ConnectorsQueue;
use Exception;
use Illuminate\Support\Facades\Cache;
use Sourcetoad\Soapy\SoapyBaseClient;
use Sourcetoad\Soapy\SoapyCurtain;
use Sourcetoad\Soapy\SoapyFacade;

class Api
{
    private array $parameters = [];

    private SoapyBaseClient $client;

    private ConnectorsQueue $connectorsQueue;

    /**
     * Api constructor.
     */
    public function __construct()
    {
        // Add access data
        $this->setParameter('KodKlienta', config('ac.login'));
        $this->setParameter('KluczSesji', config('ac.password'));

        $this->connectorsQueue = new ConnectorsQueue();
    }

    /**
     * @param int $endpoint
     */
    public function createWrapper(int $endpoint)
    {
        if ($endpoint === 1)
            $this->setParameter('KluczSesji', config('ac.password_ac_write'));

        $this->client = SoapyFacade::create(fn(SoapyCurtain $curtain) => (
        $curtain
            ->setWsdl(
                match ($endpoint) {
                    1 => config('ac.url.product-write'),
                    2 => config('ac.url.tema-write'),
                    default => config('ac.url.product-read')
                }
            )
            ->setCache(WSDL_CACHE_NONE)
        ));
    }

    /**
     * @param array $parameters
     */
    public function setParameters(array $parameters = [])
    {
        foreach ($parameters as $key => $parameter)
            $this->setParameter($key, $parameter);
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function setParameter(string $key, mixed $value)
    {
        $this->parameters[$key] = $value;
    }

    /**
     * @param $key
     * @param $value
     */
    public function addParameter($key, $value)
    {
        $this->parameters[$key][] = $value;
    }

    /**
     * @param string $method
     * @param int $ttl
     * @param int $omitCache
     * @param int $endpoint
     * @return mixed
     * @throws Exception
     */
    public function request(
        string $method,
        int    $ttl = 3660,
        int    $omitCache = 1,
        int    $endpoint = 0
    ): mixed
    {
        return $this->connectorsQueue->setting(
            config('ac.queue.enabled', false),
            config('ac.queue.max_tries', 3),
            config('ac.queue.max_running_jobs', 5),
            config('ac.queue.max_wait_seconds', 300),
            config('ac.queue.sleep_seconds', 10),
            config('ac.queue.max_per_minute', 0)
        )->call(function () use ($method, $ttl, $omitCache, $endpoint) {
            $this->createWrapper($endpoint);

            // Generate key
            $key = md5($method);

            // Check for cache
            if (Cache::has($key) && !$omitCache)
                return Cache::get($key);

            $response = $this->client->call($method, $this->parameters);

            // Save to cache
            if (!$omitCache)
                Cache::put($key, $response, $ttl);

            // Return
            return $response;
        });
    }

    /**
     * Set if you want or not to ignore queue for next requests
     * @param bool $ignoreQueueForRequests
     */
    public function setIgnoreQueueForRequests(bool $ignoreQueueForRequests)
    {
        $this->connectorsQueue->setWithQueue(!$ignoreQueueForRequests);
    }
}
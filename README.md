# Docs for PHP 8 Version

* [Docs for PHP 7.3 Version](https://bitbucket.org/eurofiranki/ac-connector/src/7.3/README.md)

## Requirements

* Laravel ^8.0
* PHP ^8.0
* ext-soap

## Install

First install package with composer:

    composer require eurofirany/ac-connector

Migrate tables for queue
    
    php artisan migrate

Next public config file to your app config folder:

    php artisan vendor:publish --tag=ac_config

This generates **ac.php** file in **config** folder

    'login' => env('AC_LOGIN'),
    'password' => env('AC_PASSWORD'),
    'password_ac_write' => env('AC_PASSWORD_WRITE'),
    'url' => [
        'product-read' => env('AC_PRODUCT_READ_URL', ''),
        'product-write' => env('AC_PRODUCT_WRITE_URL', ''),
        'tema-write' => env('TEMA_WRITE_URL', '')
    ],

    /** Queue Settings */
    'queue' => [
        'enabled' => false, // Is queue for connector enabled
        'max_tries' => 3, // Max tries for failed job
        'max_running_jobs' => 5, // Max running jobs at the same time
        'max_wait_seconds' => 300, // Wait seconds when there are too many jobs
        'sleep_seconds' => 10 // Seconds to wait between attempts
    ]


## Lumen Framework

Put this lines in **bootstrap/app.php** file:

    $app->register(\Eurofirany\AcConnector\Providers\AcConnectorServiceProvider::class);
    $app->register(\Eurofirany\Microservices\Providers\MicroservicesConnectorServiceProvider::class);

    $app->configure('ac');

For publish vendor config install **laravelista/lumen-vendor-publish** package
or just create **ac.php** file i